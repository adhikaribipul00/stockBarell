#!/bin/bash

#This script will work on downloading stock data from sec.gov
cd k10;

baseurl="https://www.sec.gov";
name=$1;
query='/cgi-bin/browse-edgar?action=getcompany&CIK='$name'&type=10-K&dateb=&owner=exclude&count=10';

#First wget the page that contains all the link
wget $baseurl$query -O $name'link.html';
#Scare the first link
new_link=`xmllint --html --xpath 'string(/html/body/div/div[4]/table/tr[2]/td[2]/a/@href)' $name'link.html' 2>/dev/null`
echo $new_link;

#Download the new page with other links
wget $baseurl$new_link -O 'new.html';

#Parse it again
new_link=`xmllint --html --xpath 'string(/html/body/div[4]/div[3]/div/table/tr[2]/td[3]/a/@href)' new.html 2>/dev/null`;

wget $baseurl$new_link -O $name'.html';




