#!/bin/bash

##Scrape from advfn website
#$1 is the character
[ -f ./stock ] && rm stock;
function func(){
	curl -s http://www.advfn.com/nasdaq/nasdaq.asp?companies=$1 > temp.html ;
	cat temp.html | awk '/<td><a href=/{print $4}' | egrep -o '[A-Z][A-Z]+' | sed 's/NASDAQ//' | sed '/^\s*$/d' >> stock;
}

for x in {A..Z}
do
	func $x
	echo "Done with $x"
done
