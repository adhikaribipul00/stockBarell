#!/bin/bash
cd ./csv

for f in *.csv
do
	name=$(basename $f .csv);
	cat $name".csv" | sed 's/time.*$//' |sed '/^\s*$/d' | sed "s/^/$name,/" >>../final.csv
done
