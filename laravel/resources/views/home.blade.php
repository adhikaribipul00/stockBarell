@include('snippets.head')
<body>
    @include('snippets.nav')

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

<div class = "container top_container">
    <div class = "row">
    <div class = "col-md-6">
    <h2>See monitored stocks</h2>
            {!! Form::open(array('url' => 'usermonitor','class'=>'form-inline my-2 my-lg-0 center')) !!}
                {!! Form::token(); !!}
                {{ Form::submit('Monitor my list',array('class'=>'btn btn-default-lg'))}}
                {!! Form::close() !!}

    </div>
    <div class = "col-md-6">

    <h2>Add stock to my portofolio</h2>
    @foreach($companies as $company)
    <h3>Add {{$company->company_name}}</h3>
            {!! Form::open(array('url' => 'portofolio/add','class'=>'form-inline my-2 my-lg-0 center')) !!}
               {{ Form::label('label', 'Volume',array('class'=>'form-control mr-sm-2 pad no-border')) }}
                {{ Form::text('volume','1',array('class'=>'form-control mr-sm-2 pad')) }}
                {{ Form::hidden('symbol',$company->symbol) }}
                {!! Form::token(); !!}
               {{ Form::label('date', 'Date',array('class'=>'form-control mr-sm-2 pad no-border')) }}
                {{ Form::date('date', \Carbon\Carbon::now(),array('class'=>'btn btn-default-lg pad')) }}
                {{ Form::submit('Add to my stocks',array('class'=>'btn btn-default-lg pad'))}}
            {!! Form::close() !!}
    @endforeach
</div>