        <!--Lets start with the basic header-->
        <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="#">Stockbarell</a>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    @unless(Auth::check())
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only"></span></a>
                    </li>
                    @endunless
                    @if(Auth::check())
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('home')}}">Home</a>
                    </li>
                    @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('home')}}">About Us</a>
                    </li>
                    @endif
                    @if(Auth::check())
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('portofolio')}}">Portofolio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('monitor')}}">Monitor</a>
                    </li>
                    @endif
                </ul>
                       <!-- <form id="search" class="form-inline my-2 my-lg-0 center" action="{{route('search')}}" method="GET">
                            <input id="seach_id" class="form-control mr-sm-2" type="text" placeholder="Search">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                            {{csrf_field()}}
                        </form> -->
                        {!! Form::open(array('url' => 'search','class'=>'form-inline my-2 my-lg-0 center')) !!}
                        {!! Form::token(); !!}
                        {{ Form::text('search_id','Ex:aapl',array('class'=>'form-control mr-sm-2')) }}
                        {{ Form::submit('Search',array('class'=>'btn btn-outline-success my-2 my-sm-0'))}}
                        {!! Form::close() !!}
                <ul class="navbar-nav ">
                    @if(Auth::check())
                    <li class="nav-item">
                        <!-- Logging out logic using a form -->
                        <a class="nav-link" href="{{route('logout')}}" onclick = "event.preventDefault();
                                                                                document.getElementById('logout-form').submit();">
                        Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>                    
                    </li>
                    @else
                    <li class="nav-item">

                        <a class="nav-link" href="{{route('login')}}">Login<span class="sr-only"></span></a>
                    </li>
                    @endif
                </ul>
            </div>
        </nav>