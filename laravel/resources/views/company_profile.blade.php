@include('snippets/head')
@include('snippets/nav')
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script> var ajax_base ="{{url('stock/price')}}" </script>
<script> var stockname ="{{$com->company_name}} " </script>
<script> var symbol ="{{$com->symbol}}" </script>
<script src ={{ asset('js/canvasjs.min.js') }}></script>
<script src ={{ asset('js/jquery.canvasjs.min.js') }}></script>
<script src ={{ asset('js/graph.js') }}></script>

<!-- Code starts here -->

<div class = "container">
    <div class = "row">
        <div class = "col-md-4 col-md-offset-2">
            <h1> {{$com->company_name}} </h1>
        </div>

        <div class = "col-md-2 col-md-offset-4">
                {!! Form::open(array('url' => 'monitor','class'=>'form-inline my-2 my-lg-0 center')) !!}
                {!! Form::token(); !!}
                {{ Form::hidden('symbol',$com->symbol) }}
                {{ Form::submit('Monitor',array('class'=>'btn btn-default-lg'))}}
                {!! Form::close() !!}
        </div>
    <div id="chartContainer" style="height: 370px; width: 100%;"></div>
</div>