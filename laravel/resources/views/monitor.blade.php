@include('snippets.head')
<body>
    @include('snippets.nav')
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src ={{ asset('js/canvasjs.min.js') }}></script>
<script src ={{ asset('js/jquery.canvasjs.min.js') }}></script>
    <div class = "container top_container">
    <div class = "row"></div>
        <div class = "row">
            <div class = "col-md-6">
            </div>
            </div>
            </div>

                <h2>Stocks Being Monitored</h2>
                <!-- For each stock generate a graph -->

               <!-- Loop through the companies being monitored -->

            <script>
            var ajax_base ="{{url('stock/price')}}" ;
            @foreach ($companies as $company)
                var data{{$company->symbol}}Points = [];
                $.getJSON(ajax_base+'/'+"{{$company->symbol}}",function(data){
                    $.each(data, function(i,field){
                        data{{$company->symbol}}Points.push({
                            x: new Date(
                                field.created_at
                            ),
                            y: [
                                field.open,
                                field.high,
                                field.low,
                                field.close
                            ]
                        });
                    });
                });
            @endforeach
            </script>

            @foreach ($companies as $company)
               <div id = "{{$company->symbol}}" style="height:400px"></div>
               <script>
                 function getname(){
                     return "{{$company->company_name}}";
                 }
                 function getsymbol(){
                     return "{{$company->symbol}}";
                 }
                 var stockname = getname();
                 var symbol = getsymbol();


        var chart{{$company->symbol}} = new CanvasJS.Chart("{{$company->symbol}}",{
            animationEnabled: true,
            theme: "light2",
            exportEnabled: true,
            title: {
                text: getname() + "stock prices"
            },
            subtitles: [{
                text: "Weekly Averages"

            }],
            axisX: {
                interval: 1,
                valueFormatString: "MMM"
            },
            axisY: {
                includeZero: false,
                prefix: "$",
                title: "Price"
            },
            toolTip: {
                content: "Date: {x}<br /><strong>Price:</strong><br />Open: {y[0]}, Close: {y[3]}<br />High: {y[1]}, Low: {y[2]}"

            },
            data: [{
                type: "candlestick",
                yValueFormatString: "##0.00",
                dataPoints: data{{$company->symbol}}Points
            }]
        });
        
                </script>
                @endforeach
                @foreach($companies as $company)
                <script>
                    chart{{$company->symbol}}.render();
                </script>
        </div>
        @endforeach 
        <!--
        </div>
            <div class = "col-md-6">
                <h2>Current Investment Portofolio</h2>
                <!-- Get a pie chart showing division of the prices -->
                <!-- Show a table showing the status of the investment 

            </div>
        </div>
        <div class = "row">
            <div class = "col-md-6">
    </div>

            </div>
            <div class = "col-md-6">
                <h2>Current Investment Portofolio</h2>
                <!-- Get a pie chart showing division of the prices -->
                <!-- Show a table showing the status of the investment 

            </div>
        </div>
        <div class = "row">
            <div class = "col-md-6">
    </div>
