@include('snippets.head')
<body>
    @include('snippets.nav')
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src ={{ asset('js/canvasjs.min.js') }}></script>
<script src ={{ asset('js/jquery.canvasjs.min.js') }}></script>

<div class = "container">
    <h1> Stocks Owned by me</h1>
    <div class = "row">
    <div class = "col-md-8 col-md-offset-2">
        <table class = "table">
        <thead> 
        <tr>
            <th scope ="col">#</th>
            <th scope ="col">Company Name</th>
            <th scope ="col">Buy date</th>
            <th scope ="col">Buy price</th>
            <th scope ="col">Volume</th>
            <th scope ="col">Total Cost</th>
            <th scope ="col">Turnover</th>
        </tr>
        <tbody>
        @for ($i = 0; $i < count($companies); $i++)
                <tr>
                <th scope = "row">{{$i+1}}</th>
                <td scope = "row">{{$companies[$i]->symbol}}</th>
                <td scope = "row">{{substr($prices[$i]->first()->created_at,0,10)}}</th>
                <td scope = "row">{{$prices[$i]->first()->high}}</th>
                <td scope = "row">{{$companies[$i]->volume}}</th>
                <td scope = "row">{{$companies[$i]->volume * $prices[$i]->first()->low}}</th>
                <td scope = "row">{{$companies[$i]->volume * $curr[$i]->first()->low - $companies[$i]->volume * $prices[$i]->first()->low}}</th>
                </tr>
        @endfor
        </tbody>
        </table>




        




