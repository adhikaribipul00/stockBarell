<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'UserController@get_home')->name('home');

Route::get('/stock/{syml}','StockController@show_company')->name('company');
Route::get('/stock/price/{syml}','StockController@get_daily_prices')->name('prices');
Route::get('/stock/csv/{syml}','StockController@ajax')->name('csv');
Route::post('/search','StockController@show_company')->name('search');
//Need to add pst routes for all the functionalities
Route::post('/monitor','UserController@add_monitor')->name('add_monitor');
Route::get('/delete/{sym}','UserController@remove_monitor')->name('remove');
Route::post('portofolio/add','UserController@remove_portofolio');
Route::post('usermonitor','UserController@user_monitor')->name('monitor');
Route::get('usermonitor','UserController@user_monitor')->name('monitor');
Route::get('portofolio/view','UserController@view_portofolio');
Route::get('portofolio/json','UserController@get_portofolio_json')->name('portofolio');


