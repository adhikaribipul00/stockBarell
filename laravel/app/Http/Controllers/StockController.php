<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Company;
use Carbon\Carbon;

class StockController extends Controller
{


    public function show_company(Request $request){
        $symbol = strtoupper($request->input('search_id'));
        $company = Company::where('symbol',$symbol)->firstOrFail();
        $stock_price = $company->prices;
        $array = array(
            'price'=>($stock_price),
            'com'=>$company,
        );
        return view('company_profile')->with($array);
    }

    public function ajax(Request $request,Response $response,$symbol){
        $fname = $symbol.".csv";
        $path = storage_path("csv/$fname");
        return response()->download($path);

    }

    public function get_daily_prices(Request $request, Response $response,$symbol){
        $company = Company::where('symbol',$symbol)->firstOrFail();
        $prices = $company->prices->where('created_at','>',new Carbon('last month'));
        //Lets create a json object
        return $prices->toJson();
    }


    //
}
