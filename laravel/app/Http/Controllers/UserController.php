<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Company;
use App\Price;
use DB;
use Carbon\Carbon;

class UserController extends Controller
{
    //Lets start with a route that shows us the basic view
    //It should show the stocks monitored by the user
    //Show the investment portofolio of the user
    /**
     * The difference between a portofolio and monitored stock
     * is that a montiored stock has 0 volume
     *
     */
    public function get_home(){
        $user =  Auth::user();
        $name = '';
        $comp = $user->companies;
        return view('home')->with('companies',$comp);
        /*
        $comp = Company::whereHas('user',function($q) use ($id) {
            $q->where('id',$id);
        })->get();
        return $comp;
        $id = User::where('username',$user->username)->firstOrFail();
        return view('home');
        */
    }

    public function user_monitor(){
        $user =  Auth::user();
        $name = '';
        $comp = $user->companies;
        return view('monitor')->with('companies',$comp);
    }

    public function add_monitor(Request $request){
        $symbol = $request->input('symbol');
        $user = Auth::user();
        $user->companies()->attach($symbol);
        return redirect()->route('home');
        //Add it to the pivot table
        //Add to the company user table


    }

    public function view_portofolio(Request $request){
        $user = Auth::user();
        $comp = $user->companies;
        foreach($comp as $c){
            echo $c->pivot->created_at;
        }
    }

    public function remove_monitor($symbol){
        $user = Auth::user();
        $user->companies()->detach($symbol);
        return 'Deleted '.$symbol;
        //Remove it from the monitor
    }

    public function remove_portofolio(Request $request){
        $symbol = $request->input('symbol');
        $date = $request->input('date');
        $new_volume = $request->input('volume');
        $user = Auth::user();

        if ($new_volume == 0){
            $user->companies()->detach($symbol);
        }
        else{
            $user->companies()->detach($symbol);
            $user->companies()->attach($symbol,array('created_at'=>$date,'volume'=>$new_volume));
        }
        return redirect('/home');
        //Remove from the portofolio
    }

    public function get_portofolio_json(){
        $user = Auth::user();
        $companies = $user->companies;
        $arr = array();
        $prices = array();
        $curr_prices = array();
        foreach($companies as $company){
            if ($company->pivot->volume > 0){
                array_push($arr,$company->pivot);
                $pr = DB::table('prices')->where([
                   [ 'symbol',$company->symbol],
                   ['created_at',$company->pivot->created_at],
                ])->get();
                array_push($prices,$pr);
                $pr_2 = DB::table('prices')->where([
                   [ 'symbol',$company->symbol],
                   ['created_at',new Carbon('yesterday')],
                ])->get();
                array_push($curr_prices,$pr_2);
            }
        }
        return view('portofolio',array('companies'=>$arr,'prices'=>$prices,'curr'=>$curr_prices));
    }

}
