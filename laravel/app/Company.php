<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';
    protected $guarded = [];
    protected $primaryKey = 'symbol';
    public $incrementing = false;

    public function prices(){
        return $this->hasMany('App\Price','symbol','symbol');
    }

    public function users(){
        //Relating to the pivot table
        return $this->belongsToMany('App\User','company_user','symbol','user_id');
    }
    
    //
}
