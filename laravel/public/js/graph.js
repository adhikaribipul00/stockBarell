window.onload = function(){    
 
var dataPoints = [];

$.getJSON(ajax_base+'/'+symbol,function(data){
$.each(data, function(i,field){
    dataPoints.push({
        x: new Date(
            field.created_at
        ),
        y: [
            field.open,
            field.high,
            field.low,
            field.close
        ]
    });
});
});

var chart = new CanvasJS.Chart("chartContainer",{
    animationEnabled: true,
    theme: "light2",
    exportEnabled: true,
    title: {
        text: stockname + "stock prices"
    },
    subtitles: [{
        text: "Weekly Averages"

    }],
    axisX: {
        interval: 1,
        valueFormatString: "MMM"
    },
    axisY: {
        includeZero: false,
        prefix: "$",
        title: "Price"
    },
    toolTip: {
        content: "Date: {x}<br /><strong>Price:</strong><br />Open: {y[0]}, Close: {y[3]}<br />High: {y[1]}, Low: {y[2]}"

    },
    data: [{
        type: "candlestick",
        yValueFormatString: "##0.00",
        dataPoints: dataPoints

    }]
});
//Lets try to replace these niggas with JSON
chart.render();
}

/*
$.get(ajax_base+'/'+symbol,getDataPointsFromCSV);

function getDataPointsFromCSV(csv){
    var csvLines = points = [];
    csvLines = csv.split(/[\r?\n|\r|\n]+/);
    for (var i = 0; i < csvLines.length;i++){
        if (csvLines[i].length > 0){
            points = csvLines[i].split(",");
			dataPoints.push({
				x: new Date(
					parseInt(points[0].split("-")[0]),
					parseInt(points[0].split("-")[1]),
					parseInt(points[0].split("-")[2])
				),
				y: [
					parseFloat(points[1]),
					parseFloat(points[2]),
					parseFloat(points[3]),
					parseFloat(points[4])
				]
			});
		}
    }
}

chart.render();


})
*/