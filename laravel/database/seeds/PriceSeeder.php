<?php

use Illuminate\Database\Seeder;
use League\Csv\Reader;

class PriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $count = 1;
        $file = public_path().'/final.csv';
        $csv = Reader::createFromPath($file);
        //$reader = Reader::createFromString('john,doe,john.doe@example.com');
        foreach($csv as $row){
            if (empty($row)) return false;
            
            \DB::table('prices')->insert(
                array(
                'symbol'=>$row[0],
                'created_at'=>$row[1],
                'open'=>$row[2],
                'high'=>$row[3],
                'low'=>$row[4],
                'close'=>$row[5],
                'volume'=>$row[6]
                )
                );
        };
        

        //
    }
}
