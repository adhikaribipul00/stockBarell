<?php

use Illuminate\Database\Seeder;
use League\Csv\Reader;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = public_path().'/final_comp.csv';
        $csv = Reader::createFromPath($file);

        foreach($csv as $row){
            if (empty($row)) return false;
            \DB::table('companies')->insert(
                array(
                    'company_name'=>$row[0],
                    'symbol'=>$row[1]
                ));
        }
    }
}
