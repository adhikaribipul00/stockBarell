<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_user', function (Blueprint $table) {
            Schema::dropIfExists('company_user');
            //$table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            //Amount of shares ==If this is 0 then its for monitoring
            $table->string('symbol');
            //>0 then its a part of user's portofolio
            $table->integer('volume')->nullable();
            //To determine if the user sold it or still retains it
            $table->softDeletes();
            //A composite primary key
            $table->primary(['symbol', 'user_id']);
            //Relating to the other two tables
        });
        //Wow this worked
        DB::statement('ALTER Table company_user add id INTEGER NOT NULL UNIQUE AUTO_INCREMENT;');
        Schema::table('company_user', function (Blueprint $table) {
            $table->foreign('symbol')->references('symbol')->on('companies');
            $table->foreign('user_id')->references('id')->on('users');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_user', function (Blueprint $table) {
            Schema::dropIfExists('company_user');
            //
        });
    }
}
