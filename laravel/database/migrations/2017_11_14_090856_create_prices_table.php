<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            Schema::dropIfExists('prices');
            $table->increments('id');
            $table->string('symbol');
            $table->foreign('symbol')->references('symbol')->on('companies');
            $table->timestamps();
            $table->double('open');
            $table->double('high');
            $table->double('low');
            $table->double('close');
            $table->double('volume');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
    }
}
